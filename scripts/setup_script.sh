#!/bin/bash 

current_dir=$(pwd)

pc_setup()
{
    ROS_WS=$HOME/j7ros_home/ros_ws
    echo "*************** INSTALLATION DIR: $ROS_WS/src/radar-safety-bubble ***************"

    cd $ROS_WS/src/radar-safety-bubble
    mkdir -p rviz/single
    mkdir -p rviz/quad

    git clone https://git.ti.com/git/mmwave_radar/mmwave_ti_ros.git
    if [ "$?" -ne "0" ]; then
        exit_setup
    fi

    cd mmwave_ti_ros
    git checkout 4d3efaaa476259a0cec09db660a21d48dd9860ca
    cd ..

    mv mmwave_ti_ros/autonomous_robotics_ros/src/turtlebot_mmwave_launchers/launch/single_sensor/bubble_visualization.rviz rviz/single/bubble_visualization.rviz
    mv mmwave_ti_ros/autonomous_robotics_ros/src/turtlebot_mmwave_launchers/launch/quad_sensor/bubble_visualization.rviz \
        mmwave_ti_ros/autonomous_robotics_ros/src/turtlebot_mmwave_launchers/launch/quad_sensor/bubble_visualization_2.rviz \
        rviz/quad/

    rm -rf mmwave_ti_ros

    cd $current_dir

    echo "Radar Safety Bubble Setup Done!"

    exit 0
}

exit_setup()
{
    echo "Setup FAILED! : Make sure you have active network connection"
    cd $current_dir
    exit 1
}

ARCH=`arch`
if [[ "$ARCH" == "x86_64" ]]; then
    pc_setup
fi

ROOT=/opt/robot
SRC=$ROOT/radar-safety-bubble/src

echo "*************** INSTALLATION DIR: $ROOT ***************"

# Create the directory path, if it does not exist
mkdir -p $SRC

# Install mmwave_ti_ros
ls $SRC | grep "mmwave_ti_ros"
if [ "$?" -ne "0" ]; then
    echo "Cloning mmwave_ti_ros."
    cd $SRC
    git clone https://git.ti.com/git/mmwave_radar/mmwave_ti_ros.git
    if [ "$?" -ne "0" ]; then
        exit_setup
    fi

    cd mmwave_ti_ros
    git checkout 4d3efaaa476259a0cec09db660a21d48dd9860ca
    cd ..

    # Apply patch
    cd $SRC/mmwave_ti_ros
    git apply $ROOT/radar-safety-bubble/patches/mmwave_ti_ros.patch
    cd $ROOT

    # Grab what we need
    mv $SRC/mmwave_ti_ros/autonomous_robotics_ros/src/ti_mmwave_rospkg \
        $SRC/mmwave_ti_ros/autonomous_robotics_ros/src/ti_safety_bubble \
        $SRC/mmwave_ti_ros/autonomous_robotics_ros/src/turtlebot_mmwave_launchers \
        $SRC/mmwave_ti_ros/

    # Remove unnecessary files
    rm -rf $SRC/mmwave_ti_ros/autonomous_robotics_ros
    rm -rf $SRC/mmwave_ti_ros/ros_driver
fi

# Install Turtlebot Packages

# Install turtlebot
ls $SRC | grep "turtlebot"
if [ "$?" -ne "0" ]; then
    echo "Cloning turtlebot."
    cd $SRC
    git clone https://github.com/turtlebot/turtlebot.git
    if [ "$?" -ne "0" ]; then
        exit_setup
    fi

    cd turtlebot
    git checkout bd830d8
    cd $ROOT
fi

# Install turtlebot_apps
ls $SRC | grep "turtlebot_apps"
if [ "$?" -ne "0" ]; then
    echo "Cloning turtlebot_apps."
    cd $SRC
    git clone https://github.com/turtlebot/turtlebot_apps.git
    if [ "$?" -ne "0" ]; then
        exit_setup
    fi

    cd turtlebot_apps
    git checkout 4bbafc7
    cd $ROOT
fi

# Install turtlebot_msgs
ls $SRC | grep "turtlebot_msgs"
if [ "$?" -ne "0" ]; then
    echo "Cloning turtlebot_msgs."
    cd $SRC
    git clone https://github.com/turtlebot/turtlebot_msgs.git
    if [ "$?" -ne "0" ]; then
        exit_setup
    fi

    cd turtlebot_msgs
    git checkout 834c448
    cd $ROOT
fi

# Install turtlebot_simulator
ls $SRC | grep "turtlebot_simulator"
if [ "$?" -ne "0" ]; then
    echo "Cloning turtlebot_simulator."
    cd $SRC
    git clone https://github.com/turtlebot/turtlebot_simulator.git
    if [ "$?" -ne "0" ]; then
        exit_setup
    fi

    cd turtlebot_simulator
    git checkout beae4b3
    cd $ROOT
fi

# Install navigation packages
ls $SRC | grep "navigation"
if [ "$?" -ne "0" ]; then
    echo "Cloning navigation."
    cd $SRC
    mkdir temp
    cd temp
    git clone https://github.com/ros-planning/navigation.git
    if [ "$?" -ne "0" ]; then
        exit_setup
    fi

    cd navigation
    git checkout 00fa9d1

    # Apply patch
    git apply $ROOT/radar-safety-bubble/patches/navigation_ti_safety_bubble.patch
    cd $ROOT

    # Grab what we need
    mkdir $SRC/navigation
    mv $SRC/temp/navigation/move_base/ \
        $SRC/temp/navigation/rotate_recovery/ \
        $SRC/navigation

    # Remove unnecessary files
    rm -rf $SRC/temp
fi

# Install kobuki packages

# Install kobuki
ls $SRC | grep "kobuki"
if [ "$?" -ne "0" ]; then
    echo "Cloning kobuki."
    cd $SRC
    mkdir temp
    cd temp
    git clone https://github.com/yujinrobot/kobuki.git
    if [ "$?" -ne "0" ]; then
        exit_setup
    fi

    cd kobuki
    git checkout 23748ed
    cd $ROOT

    # Grab what we need
    mkdir $SRC/kobuki
    mv $SRC/temp/kobuki/kobuki_description/ \
        $SRC/temp/kobuki/kobuki_node/ \
        $SRC/temp/kobuki/kobuki_keyop/ \
        $SRC/temp/kobuki/kobuki_safety_controller/ \
        $SRC/temp/kobuki/kobuki_bumper2pc/ \
        $SRC/kobuki

    # Remove unnecessary files
    rm -rf $SRC/temp
fi

# Install kobuki_gazebo_plugins
ls $SRC | grep "kobuki_gazebo_plugins"
if [ "$?" -ne "0" ]; then
    echo "Cloning kobuki_desktop."
    cd $SRC
    git clone https://github.com/yujinrobot/kobuki_desktop.git
    if [ "$?" -ne "0" ]; then
        exit_setup
    fi

    cd kobuki_desktop
    git checkout ea5b728
    cd ..

    # Grab what we need
    mv kobuki_desktop/kobuki_gazebo_plugins .

    # Remove unnecessary files
    rm -rf kobuki_desktop
    cd $ROOT
fi

# Install yujin_ocs
ls $SRC | grep "yujin_ocs"
if [ "$?" -ne "0" ]; then
    echo "Cloning yujin_ocs."
    cd $SRC
    mkdir temp
    cd temp
    git clone https://github.com/yujinrobot/yujin_ocs.git
    if [ "$?" -ne "0" ]; then
        exit_setup
    fi

    cd yujin_ocs
    git checkout 17337e5
    cd $ROOT

    # Grab what we need
    mkdir $SRC/yujin_ocs
    mv $SRC/temp/yujin_ocs/yocs_cmd_vel_mux/ \
        $SRC/temp/yujin_ocs/yocs_controllers/ \
        $SRC/yujin_ocs

    # Remove unnecessary files
    rm -rf $SRC/temp
fi

echo "Radar Safety Bubble Setup Done!"
